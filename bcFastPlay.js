console.log('Is it even loaded ?');
window.onkeydown = function(e)
{
  if (e.keyCode == 32 && e.target == document.body) // SPACE BAR !
  {
    let playButton = document.getElementsByClassName('playbutton')[0];
    let playButtonClassName = playButton.className;
    if(playButtonClassName == 'playbutton')
    {
      console.log(playButtonClassName + ' : GO PLAY');
      playButton.click();
    }
    else if (playButtonClassName == 'playbutton playing')
    {
      console.log(playButtonClassName + ' : GO PAUSE');
      playButton.click();
    }
  }
  else if (e.keyCode == 81 && e.target == document.body) // Q !
  {
    let prevButton = document.getElementsByClassName('prevbutton')[0];
    let prevButtonClassName = prevButton.className;
    if(prevButtonClassName == 'prevbutton')
    {
      console.log(prevButtonClassName + ' : GO BACK');
      prevButton.click();
    }
  }
  else if (e.keyCode == 68 && e.target == document.body) // D !
  {
    let nextButton = document.getElementsByClassName('nextbutton')[0];
    let nextButtonClassName = nextButton.className;
    if(nextButtonClassName == 'nextbutton')
    {
      console.log(nextButtonClassName + ' : GO NEXT');
      nextButton.click();
    }
  }
  e.preventDefault();
};
